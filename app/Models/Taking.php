<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Taking extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','photo'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function takingdetail(){
        return $this->hasMany(TakingDetail::class);
    }
}
