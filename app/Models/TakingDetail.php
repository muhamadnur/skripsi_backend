<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TakingDetail extends Model
{
    use HasFactory;
    protected $table = "taking_details";
    protected $fillable = ['taking_id','name','qty','satuan'];

    public function taking(){
        return $this->belongsTo(Taking::class);
    }
 
}
