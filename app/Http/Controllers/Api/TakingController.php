<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Taking;
use App\Models\TakingDetail;
use Countable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Constraint\Count;

class TakingController extends Controller
{
    public function create(Request $request){
        $taking = new Taking();
        $taking->user_id = Auth::user()->id;

        //check if taking has photo
        if($request->photo != ''){
            //choose a unique name for photo
            $photo = time().'.jpg';
            file_put_contents('storage/taking/'.$photo,base64_decode($request->photo));
            $taking->photo = $photo;
            }
        // mistake
        $taking->save();
        if(count($request['name']>0)){
            foreach ($request['name'] as $item=>$v) {
                $data = array(
                    'taking_id'=>$taking->id,
                    'name'=>$request['name'][$item],
                    'qty'=>$request['qty'][$item],
                    'satuan'=>$request['satuan'][$item],
                );
                TakingDetail::create($data);
            }
        }
        $taking->takingdetail;
        $taking->user;

        return response()->json([
            'success' => true,
            'message' => 'takinged',
            'taking' => $taking
        ]);
    }

    public function takings(){
        $taking = Taking::orderBy('id','desc')->get();
        foreach($taking as $tk){
            //get user of taking
            $tk->takingdetail;
        }

        return response()->json([
            'success' => true,
            'taking' => $tk
        ]);
    }

}
