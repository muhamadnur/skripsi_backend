<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\PresenceController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\TakingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

Route::post('login',[AuthController::class,'login']);
Route::post('register',[AuthController::class,'register']);
Route::get('logout',[AuthController::class,'logout'])->middleware('jwtAuth');
Route::post('update',[AuthController::class,'updateUser']);

Route::get('presence',[PresenceController::class,'presence'])->middleware('jwtAuth');
Route::post('presence/create',[PresenceController::class,'create'])->middleware('jwtAuth');

//Product
Route::get('product',[ProductController::class,'product']);
Route::post('product/create',[ProductController::class,'create']);

Route::post('taking/create',[TakingController::class,'create']);
Route::get('taking',[TakingController::class,'takings']);
