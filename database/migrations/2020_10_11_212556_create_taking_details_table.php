<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTakingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taking_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("taking_id");
            $table->string("name");
            $table->string("qty");
            $table->string("satuan");
            $table->timestamps();

            $table->foreign('taking_id')->references('id')->on('takings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taking_details');
    }
}
